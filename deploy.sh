#!/bin/bash
echo '\n\n----------------------------------------------------'
echo 'Stopping app'
docker stop $(docker ps -q)

echo '\n\n----------------------------------------------------'
echo 'Building app'
docker-compose -f docker-compose.production.yml build

echo '\n\n----------------------------------------------------'
echo 'Running migrations'
docker-compose -f docker-compose.production.yml run app bin/rake db:migrate RAILS_ENV=production
docker-compose -f docker-compose.production.yml down

echo '\n\n----------------------------------------------------'
echo 'Starting app'
docker-compose -f docker-compose.production.yml up -d
echo Finished!

