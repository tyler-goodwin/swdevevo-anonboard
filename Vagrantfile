# -*- mode: ruby -*-
# vi: set ft=ruby :

def provision_docker(config)
  config.vm.provision 'shell', inline: <<-SHELL
    echo 'Install prereqs'
    sudo apt update

    sudo apt-get --assume-yes install \
                 apt-transport-https \
                 ca-certificates \
                 curl \
                 software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    echo 'Install docker'
    sudo apt-get update
    sudo apt-get --assume-yes install -y docker-ce

    echo 'install compose'
    sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose

    echo 'add user to group'
    sudo usermod -aG docker $USER
    
    echo 'finished!'
    exit 0
  SHELL
end

def provision_ruby(config)
  config.vm.provision 'shell', inline: <<-SHELL
    curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo sh -c "echo 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main' > /etc/apt/sources.list.d/pgdg.list"
    wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -


    sudo apt-get update
    sudo apt-get install -y git-core \
                            curl \
                            zlib1g-dev \
                            build-essential \
                            libssl-dev \
                            libreadline-dev \
                            libyaml-dev \
                            libsqlite3-dev \
                            sqlite3 \
                            libxml2-dev \
                            libxslt1-dev \
                            libcurl4-openssl-dev \
                            libpq-dev \
                            libgdbm-dev \
                            software-properties-common \
                            libffi-dev \
                            libncurses5-dev \
                            automake \
                            libtool \
                            bison \
                            nodejs \
                            yarn

    [ -d ~/.rvm ] && rm -rf ~/.rvm
    gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
    curl -sSL https://get.rvm.io | bash -s stable
    source ~/.rvm/scripts/rvm
    source /etc/profile.d/rvm.sh
    rvm install 2.5.1
    rvm use 2.5.1 --default

    gem install bundler
    gem install rails -v 5.2.0

    sudo usermod -aG rvm $USER

    exit 0
  SHELL
end

Vagrant.configure("2") do |config|
  # Install docker & compose 
  config.vm.box = "ubuntu/xenial64"
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # provision_docker config
  provision_docker config 
  config.vm.synced_folder ".", "/var/app/", create: true

  config.vm.provision 'shell', run: 'always', inline: "[ ! -f /var/app/tmp/pids/server.pid ] || rm /var/app/tmp/pids/server.pid"
  config.vm.provision 'shell', run: 'always', inline: "cd /var/app && docker-compose up -d"
  config.vm.network "private_network", ip: "192.168.10.10"
end
