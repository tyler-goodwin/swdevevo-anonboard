if ENV['USE_INFLUX']
  InfluxDB::Rails.configure do |config|
    config.influxdb_database = ENV["INFLUX_DB"]
    config.influxdb_username = ENV["INFLUX_USERNAME"]
    config.influxdb_password = ENV["INFLUX_PASSWORD"]
    config.influxdb_hosts    = [ENV["INFLUX_HOST"]]
    config.influxdb_port     = 8086
  end
end