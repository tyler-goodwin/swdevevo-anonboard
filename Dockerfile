FROM ruby:2.5.1-alpine

RUN apk --update --upgrade add ruby-dev libxml2-dev libxslt-dev pcre-dev libffi-dev nodejs postgresql-dev build-base libcurl curl-dev tzdata

RUN mkdir -p /var/app
COPY Gemfile /var/app/Gemfile
COPY Gemfile.lock /var/app/Gemfile.lock
WORKDIR /var/app
RUN bundle install
COPY . /var/app/
RUN bundle exec rake DATABASE_URL=postgresql:does_not_exist assets:precompile
CMD bin/rails s -b 0.0.0.0